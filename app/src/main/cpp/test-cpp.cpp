//
// Created by ggerganov on 26.04.21.
//

#include "test-cpp.h"
#include "plog/Formatters/FuncMessageFormatter.h"

#include <android/log.h>
#include <jni.h>

#include <ggwave/ggwave.h>
#include <crypkt/crypkt.hpp>

namespace {
    JavaVM* g_jvm;
    jobject g_mainObject;
    ggwave_Instance g_ggwave;
    std::shared_ptr<CryptoProtocol> g_CryptoProtocol = nullptr;
}

extern "C"
JNIEXPORT void JNICALL
Java_com_example_ggwave_MainActivity_initNative(JNIEnv * env, jobject obj) {
    __android_log_print(ANDROID_LOG_DEBUG, "ggwave (native)", "Initializing native module");

    // Initialize logging with PLOG? Maybe in the future
    //static plog::AndroidAppender<plog::FuncMessageFormatter> appender("crypkt");
    //plog::init(plog::debug, &appender);

    ggwave_Parameters parameters = ggwave_getDefaultParameters();
    parameters.sampleFormatInp = GGWAVE_SAMPLE_FORMAT_I16;
    parameters.sampleFormatOut = GGWAVE_SAMPLE_FORMAT_I16;
    parameters.sampleRateInp = 48000;
    parameters.payloadLength = PAYLOAD;
    g_ggwave = ggwave_init(parameters);

    g_CryptoProtocol = std::make_shared<CryptoProtocol>();
    assert(g_CryptoProtocol->checkPacketSize());

    switch (g_CryptoProtocol->initialize()) {
        case 0:
            // everything fine :)
            break;
        case 1:
            __android_log_print(ANDROID_LOG_DEBUG, "ggwave (native)", "Sodium reinitialized");
            break;
        case -1:
            __android_log_print(ANDROID_LOG_ERROR, "ggwave (native)", "Error initializinf sodium. Sayonara!");
        default:
            __android_log_print(ANDROID_LOG_ERROR, "ggwave (native)", "Misterious sodium error. Sayonara!");
            assert(false);
    }

    env->GetJavaVM(&g_jvm);
    g_mainObject = env->NewGlobalRef(obj);
}


extern "C"
JNIEXPORT void JNICALL
Java_com_example_ggwave_MainActivity_newProfile(JNIEnv *env, jobject thiz) {
    g_CryptoProtocol->generateKeyPair();
}

extern "C"
JNIEXPORT void JNICALL
Java_com_example_ggwave_MainActivity_processCaptureData(JNIEnv *env, jobject thiz, jshortArray data) {
    jsize dataSize = env->GetArrayLength(data);
    //__android_log_print(ANDROID_LOG_DEBUG, "ggwave (native)", "datasize is: '%d'", dataSize);

    jboolean isCopy = false;
    jshort * cData = env->GetShortArrayElements(data, &isCopy);
    //jlong sum = 0;
    //for (int i=0; i<dataSize; i++)
    //    sum += cData[i];
    //__android_log_print(ANDROID_LOG_DEBUG, "ggwave (native)", "sum is: '%d' zero val is %d", sum, cData[0]);

    char output[256];
    int ret = ggwave_decode(g_ggwave, (char *) cData, 2*dataSize, output);
    env->ReleaseShortArrayElements(data, cData,0);
    //__android_log_print(ANDROID_LOG_DEBUG, "ggwave (native)", "I tried with: '%d'", 2*dataSize);

    if (ret != 0) {
        // __android_log_print(ANDROID_LOG_DEBUG, "ggwave (native)", "Received message: '%s'", output);

        g_CryptoProtocol->receiveQueue( reinterpret_cast<unsigned char*>(&output) , ret);

        char message[MESSAGE_LEN_E] = {0};
        size_t msg_len = 0;
        packetType p;
        packetParsingErrors err;
        while (g_CryptoProtocol->processIncomingPackets(p, message, msg_len, err)) {
            if (p == packetType::MESSAGE) {
                // __android_log_print(ANDROID_LOG_DEBUG, "ggwave (native)", "I received a message! %d long and is '%s'", msg_len, message);
            } else if (p == packetType::KEYS) {
                __android_log_print(ANDROID_LOG_DEBUG, "ggwave (native)", "Received KEYS!!");
            } else if (p == packetType::NONCE) {
                __android_log_print(ANDROID_LOG_DEBUG, "ggwave (native)", "Received NONCE!!");
            } else {
                __android_log_print(ANDROID_LOG_DEBUG, "ggwave (native)", "Invalid packet?!");
            }
            jclass handlerClass = env->GetObjectClass(g_mainObject);
            jmethodID mid_onReceivedMessage = env->GetMethodID(handlerClass, "onNativeReceivedMessage",
                                                               "(I[B)V");

            jbyteArray jba_message = env->NewByteArray(msg_len);
            env->SetByteArrayRegion(jba_message, 0, msg_len, (jbyte*) message);

            env->CallVoidMethod(g_mainObject, mid_onReceivedMessage, (int) p, jba_message);

            env->DeleteLocalRef(jba_message);
            env->DeleteLocalRef(handlerClass);
        }
    }
}

extern "C"
JNIEXPORT jint JNICALL
Java_com_example_ggwave_MainActivity_sendMessage(JNIEnv *env, jobject thiz, jstring message,
                                                 jint type) {
    //__android_log_print(ANDROID_LOG_DEBUG, "ggwave (native)", "Encoding message: '%s'", env->GetStringUTFChars(message, NULL));

    // push to packets - https://gist.github.com/mondain/9ba53a100019406d29dd1299819013d5

    commands cmd = (commands) type;
    switch (cmd) {
        case CMD_KEYS:
            __android_log_print(ANDROID_LOG_INFO, "ggwave (native)", "Sending keys");
            break;
        case CMD_NONCE:
            __android_log_print(ANDROID_LOG_INFO, "ggwave (native)", "Sending nonce");
            break;
        case CMD_MESSAGE:
            __android_log_print(ANDROID_LOG_INFO, "ggwave (native)", "Sending message");
            break;
        default:
            __android_log_print(ANDROID_LOG_ERROR, "ggwave (native)", "Message type not understood");
            return -1;
    }

    packetGenerationErrors err;
    g_CryptoProtocol->prepareMessage(cmd,
                                     (const unsigned char*) env->GetStringUTFChars(message, NULL),
                                     env->GetStringLength(message),
                                     err);
    if (err != PGE_NONE) {
        return (jint) err;
    }
    __android_log_print(ANDROID_LOG_DEBUG, "ggwave (native)", "Got eroror: '%d'", err);
    __android_log_print(ANDROID_LOG_DEBUG, "ggwave (native)", "Status is: '%d'", g_CryptoProtocol->checkStatus());
    size_t i = 0;

    char encoded_message[PAYLOAD];
    char* waveform = NULL;
    size_t cur_pos = 0;
    while (true) {
        i = g_CryptoProtocol->popPacket(encoded_message);

        // __android_log_print(ANDROID_LOG_DEBUG, "ggwave (native)", "Popped crypted message: '%d'", i);
        if (i == 0) break; // are we done?

        const int n = ggwave_encode(g_ggwave, encoded_message,
                                    i, GGWAVE_TX_PROTOCOL_AUDIBLE_FAST,
                                    10, NULL, 1);


        //__android_log_print(ANDROID_LOG_DEBUG, "ggwave (native)", "requested? %d", (cur_pos + n));
        waveform = (char *) realloc(waveform, (cur_pos + n) * sizeof (char));
        if (waveform == NULL) {
            // reallocation failed
            __android_log_print(ANDROID_LOG_DEBUG, "ggwave (native)", "Cannot allocate these many chars: %d", (cur_pos + n));
            return -1;
        }

        //__android_log_print(ANDROID_LOG_DEBUG, "ggwave (native)", "allocated? %p", waveform);

        const int ret = ggwave_encode(g_ggwave, encoded_message,
                                      i,
                                      GGWAVE_TX_PROTOCOL_AUDIBLE_FAST, 10, &(waveform[cur_pos]), 0);

        if (2 * ret != n) {
            __android_log_print(ANDROID_LOG_ERROR, "ggwave (native)", "Failed to encode message");
            if (waveform != NULL) free(waveform);
            return -2;
        }
        cur_pos += n;
    }

    // call back
    jclass handlerClass = env->GetObjectClass(g_mainObject);
    jmethodID mid_onMessageEncoded = env->GetMethodID(handlerClass, "onNativeMessageEncoded","([S)V");

    jshortArray jba_message = env->NewShortArray(cur_pos);

    env->SetShortArrayRegion(jba_message, 0, cur_pos/2, (jshort *) waveform);
    env->CallVoidMethod(g_mainObject, mid_onMessageEncoded, jba_message);
    env->DeleteLocalRef(jba_message);

    free(waveform);
    return 0;
}

extern "C"
JNIEXPORT bool JNICALL
Java_com_example_ggwave_MainActivity_loadProfile(JNIEnv *env, jobject thiz, jstring profileMe, jstring profileBob, jstring password) {

    std::string me, bob, pass;

    me.assign(  env->GetStringUTFChars(profileMe, NULL) , env->GetStringLength(profileMe) );
    bob.assign( env->GetStringUTFChars(profileBob, NULL), env->GetStringLength(profileBob) );
    pass.assign(env->GetStringUTFChars(password, NULL) , env->GetStringLength(password) );


    return g_CryptoProtocol->loadMe(me, pass) && g_CryptoProtocol->loadBob(bob, pass);

}

extern "C"
JNIEXPORT bool JNICALL
Java_com_example_ggwave_MainActivity_saveProfile(JNIEnv *env, jobject thiz, jstring profileName, jstring profilePassword) {

    std::string me, bob, name, pass;
    name.assign(env->GetStringUTFChars(profileName, NULL) , env->GetStringLength(profileName) );
    pass.assign(env->GetStringUTFChars(profilePassword, NULL) , env->GetStringLength(profilePassword) );

    std::string meData, bobData;

    bool rme = g_CryptoProtocol->saveMe(meData, name, pass);
    bool rbob = g_CryptoProtocol->saveBob(bobData, name, pass);

    if (!(rme || rbob)) {
        return false;
    }

    jstring myProfile = env->NewStringUTF(meData.c_str());
    jstring bobProfile = env->NewStringUTF(bobData.c_str());


    // call native method to store encrypted data to file
    jclass handlerClass = env->GetObjectClass(g_mainObject);
    jmethodID mid_onNativeSaveProfile = env->GetMethodID(handlerClass, "onNativeSaveProfile",
                                                         "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V");

    env->CallVoidMethod(g_mainObject, mid_onNativeSaveProfile, profileName, myProfile, bobProfile);

    env->DeleteLocalRef(myProfile);
    env->DeleteLocalRef(bobProfile);
    return true;
}


extern "C"
JNIEXPORT jint JNICALL
Java_com_example_ggwave_MainActivity_checkStatus(JNIEnv *env, jobject thiz) {
    return (jint) g_CryptoProtocol->checkStatus();
}
