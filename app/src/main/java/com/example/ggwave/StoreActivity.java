package com.example.ggwave;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class StoreActivity extends AppCompatActivity {
    protected List<String> values = new ArrayList<String>();
    protected ArrayAdapter<String> adapter;

    private void deleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles())
                deleteRecursive(child);

        fileOrDirectory.delete();
    }

    private void updateList(){
        values.clear();
        File f = new File(getFilesDir().toURI());
        File[] dirs = f.listFiles(File::isDirectory);
        for (File d : dirs) values.add(d.getName());
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store);

        Button buttonLoad=(Button)findViewById(R.id.buttonLoad);
        buttonLoad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                EditText profileNameEditText = (EditText) findViewById(R.id.textProfileName);
                EditText profilePassEditText = (EditText) findViewById(R.id.textProfilePassword);

                String name = profileNameEditText.getText().toString();
                String pass = profilePassEditText.getText().toString();

                if ((name == "") || (pass == "")) {
                    Toast.makeText(view.getContext(), "No profile and/or password specified!",
                            Toast.LENGTH_LONG).show();
                    return;
                }

                if (!values.contains(name)) {
                    // error
                    Toast.makeText(view.getContext(), "Profile does not exists!",
                            Toast.LENGTH_LONG).show();
                    return;
                }


                Intent mainIntent = new Intent(view.getContext(), MainActivity.class);
                mainIntent.putExtra("profileName", name);
                mainIntent.putExtra("profilePass", pass);
                mainIntent.putExtra("isNew", false);
                startActivity(mainIntent);

            }
        });

        Button buttonDelete=(Button)findViewById(R.id.buttonDelete);
        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                EditText profileNameEditText = (EditText) findViewById(R.id.textProfileName);
                String name = profileNameEditText.getText().toString();

                File f = new File(getFilesDir().toURI());
                File[] dirs = f.listFiles(File::isDirectory);
                for (File d : dirs) {
                    //Log.v("ggwave", "Store checking " + name + " vs " + d.getName());
                    if (d.getName().equals(name)) {
                        deleteRecursive(d);
                        updateList();
                        adapter.notifyDataSetChanged();
                        return;
                    }
                }
                // if we reach this point we did not find the profile.
                Toast.makeText(view.getContext(), "Profile does not exists!",
                        Toast.LENGTH_LONG).show();
            }
        });

        Button buttonSave=(Button)findViewById(R.id.buttonNew);
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                EditText profileNameEditText = (EditText) findViewById(R.id.textProfileName);
                EditText profilePassEditText = (EditText) findViewById(R.id.textProfilePassword);

                String name = profileNameEditText.getText().toString();
                String pass = profilePassEditText.getText().toString();

                if ((name == "") || (pass == "")) {
                    Toast.makeText(view.getContext(), "No profile and/or password specified!",
                            Toast.LENGTH_LONG).show();
                    return;
                }

                // Check profile already exists
                if (values.contains(name)) {
                    // error
                    Toast.makeText(view.getContext(), "Profile already exists!",
                            Toast.LENGTH_LONG).show();
                    return;
                }

                //Intent returnIntent = new Intent();
                Intent mainIntent = new Intent(view.getContext(), MainActivity.class);
                mainIntent.putExtra("profileName", name);
                mainIntent.putExtra("profilePass", pass);
                mainIntent.putExtra("isNew", true);
                startActivity(mainIntent);

            }
        });

        ListView profileList=(ListView) findViewById(R.id.listProfileSelect);

        //now presenting the data into screen
        adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item, values);
        profileList.setAdapter(adapter);//setting the adapter

        profileList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                String s = profileList.getItemAtPosition(i).toString();
                ((EditText) findViewById(R.id.textProfileName)).setText(s);
                ((EditText) findViewById(R.id.textProfilePassword)).setText("");

            }
        });


    }

    @Override
    protected void onResume() {
        //
        super.onResume();

        updateList();

        adapter.notifyDataSetChanged();
    }
}
