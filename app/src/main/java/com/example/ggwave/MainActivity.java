package com.example.ggwave;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder;
import android.os.Bundle;

import android.os.Handler;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;
import android.widget.ViewSwitcher;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ShortBuffer;


public class MainActivity extends AppCompatActivity {

    private CapturingThread mCapturingThread;
    private PlaybackThread mPlaybackThread;
    private static final int REQUEST_RECORD_AUDIO = 13;

    // Commands enum
    private static final int CMD_KEYS = 0;
    private static final int CMD_MESSAGE = 1;
    private static final int CMD_NONCE = 2;

    // Native interface:
    private native void initNative();

    private native void newProfile();

    private native void processCaptureData(short[] data);

    private native int sendMessage(String message, int type);

    private native boolean loadProfile(String profileMe, String profileBob, String password);

    private native boolean saveProfile(String profileName, String password);

    private native int checkStatus();

    private static String profileName;
    private static String profilePass;

    // Native callbacks:
    private void onNativeReceivedMessage(int c_type,byte c_message[]) {
        String message = new String(c_message);
        Log.v("ggwave", "Received message: " + message);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (c_type == 0) {
                    Toast.makeText(MainActivity.this, "Error!",
                            Toast.LENGTH_LONG).show();
                } else if (c_type == 1) {
                    Toast.makeText(MainActivity.this, "Got keys!",
                            Toast.LENGTH_LONG).show();
                } else if (c_type == 2) {
                    Toast.makeText(MainActivity.this, "Got Nonce!",
                            Toast.LENGTH_LONG).show();
                } else if (c_type == 3) {
                    TextView textView = (TextView) findViewById(R.id.textViewReceived);
                    textView.setText(message);
                }
            }
        });
    }

    private void onNativeMessageEncoded(short c_message[]) {
        Log.v("ggwave", "Playing encoded message ..");
        mPlaybackThread = new PlaybackThread(c_message, new PlaybackListener() {
            @Override
            public void onProgress(int progress) {
                // todo : progress updates
            }

            @Override
            public void onCompletion() {
                mPlaybackThread.stopPlayback();
                ((Button) findViewById(R.id.buttonTogglePlayback)).setText("Send");
                ((TextView) findViewById(R.id.textViewStatus)).setText("Status: Idle");
            }
        });
    }

    private void freeze() {
        findViewById(R.id.buttonLoadSaveProfile).setEnabled(false);
        findViewById(R.id.buttonToggleCapture).setEnabled(false);
        findViewById(R.id.buttonToggleCaptureFirst).setEnabled(false);
        findViewById(R.id.buttonToggleKeys).setEnabled(false);
        findViewById(R.id.buttonToggleKeysFirst).setEnabled(false);
        findViewById(R.id.buttonTogglePlayback).setEnabled(false);
        findViewById(R.id.buttonDoneSetup).setEnabled(false);
    }
    private void unfreeze() {
        findViewById(R.id.buttonLoadSaveProfile).setEnabled(true);
        findViewById(R.id.buttonToggleCapture).setEnabled(true);
        findViewById(R.id.buttonToggleCaptureFirst).setEnabled(true);
        findViewById(R.id.buttonToggleKeys).setEnabled(true);
        findViewById(R.id.buttonToggleKeysFirst).setEnabled(true);
        findViewById(R.id.buttonTogglePlayback).setEnabled(true);
        //findViewById(R.id.buttonDoneSetup).setEnabled(true);
    }

    private void saveFile(String dir, String name, String content) throws IOException{
        File test = new File(getFilesDir() + File.separator + dir, name);
        test.getParentFile().mkdirs();
        test.createNewFile();
        FileOutputStream fos = new FileOutputStream(test);
        fos.write(content.getBytes());
        fos.close();
        //Log.v("ggwave", "Saving file profile " + dir + " : " + name);
    }

    private String loadFile(String dir, String name){

        try {
            File newFile = new File(getFilesDir() + File.separator + dir,name);
            FileInputStream fis = new FileInputStream(newFile);

            StringBuffer fileContent = new StringBuffer("");

            byte[] buffer = new byte[1024];
            int n;
            while ((n = fis.read(buffer)) != -1) {
                fileContent.append(new String(buffer, 0, n));
            }
            return fileContent.toString();

        } catch (IOException e) {
            e.printStackTrace();
            //Log.v("ggwave", "Error loading profile " + dir + " : " + name);
        }
        return "";
    }

    private void onNativeSaveProfile( String profileName, String myProfile, String bobProfile) throws IOException {
        Log.v("ggwave", "saving profile ..");
        saveFile(profileName,profileName + ".me", myProfile);
        saveFile(profileName,profileName + ".bob", bobProfile);
    }

    private void onLoadProfile( String profileName, String profilePassword) {
        Log.v("ggwave", "loading profile ..");

        String myProfile=loadFile(profileName,profileName+".me");
        String bobProfile=loadFile(profileName,profileName+".bob");

        if (myProfile == "" || bobProfile == ""){
            runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MainActivity.this, "Loading profile FAILED! Could not read files.",
                        Toast.LENGTH_LONG).show();
                finish();
            }
        });
        } else {
            boolean r = loadProfile(myProfile, bobProfile, profilePassword);
            if (!r){
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(MainActivity.this, "Loading profile FAILED! Wrong password?",
                                Toast.LENGTH_LONG).show();
                        finish();
                    }
                });
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        System.loadLibrary("ggwave");
        System.loadLibrary("sodium");
        System.loadLibrary("crypkt");
        System.loadLibrary("test-cpp");
        initNative();

        // load profile?
        profileName = getIntent().getExtras().getString("profileName");
        profilePass = getIntent().getExtras().getString("profilePass");
        boolean isNewProfile = getIntent().getExtras().getBoolean("isNew");


        // view switcher
        ViewFlipper viewFlip = (ViewFlipper) findViewById(R.id.viewFlipper);

        if (!isNewProfile) {
            Log.v("ggwave", "Calling onLoadProfile");
            freeze();
            Toast.makeText(MainActivity.this, "Loading profile, be patient...",
                    Toast.LENGTH_LONG).show();

            new Thread( new Runnable() { @Override public void run() {
                if (! isNewProfile) onLoadProfile(profileName,profilePass);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        unfreeze();
                        //Log.v("ggwave", "Status is" + checkStatus());
                        if (checkStatus() == 2){
                            viewFlip.setDisplayedChild(2);
                        } else {
                            viewFlip.setDisplayedChild(1);
                        }
                    }
                });
            } } ).start();
        } else {
            newProfile();
            viewFlip.setDisplayedChild(1);
        }

        //kFilePath = getRecordingFilePath();
        mCapturingThread = new CapturingThread(new AudioDataReceivedListener() {
            @Override
            public void onAudioDataReceived(short[] data) {
                //Log.v("ggwave", "java: 0 = " + data[0]);
                processCaptureData(data);
            }
        });

        ((Button)findViewById(R.id.buttonDoneSetup)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // Show Next View
                Log.v("ggwave", "Switching view");
                viewFlip.setDisplayedChild(2);
            }
        });
        ((Button)findViewById(R.id.buttonMessedUpSetup)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                finish();
            }
        });

        ((TextView) findViewById(R.id.textViewReceived)).setMovementMethod(new ScrollingMovementMethod());


        Button buttonStartLoadSaveActivity=(Button)findViewById(R.id.buttonLoadSaveProfile);
        buttonStartLoadSaveActivity.setOnClickListener(new View.OnClickListener(){
                                            @Override
                                            public void onClick(View view){
                                                Log.v("ggwave", "Calling saveProfile");
                                                freeze();
                                                viewFlip.setDisplayedChild(0);

                                                Toast.makeText(MainActivity.this, "Saving profile...",
                                                        Toast.LENGTH_LONG).show();
                                                new Thread( new Runnable() { @Override public void run() {
                                                    saveProfile(profileName, profilePass);
                                                    runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            viewFlip.setDisplayedChild(2);
                                                            unfreeze();
                                                        }
                                                    });
                                                } } ).start();
                                            }
                                        }
        );




        View.OnClickListener capture = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mCapturingThread.capturing()) {
                    startAudioCapturingSafe();
                } else {
                    mCapturingThread.stopCapturing();
                }

                if (mCapturingThread.capturing()) {
                    ((Button) findViewById(R.id.buttonToggleCapture)).setText("Stop");
                    ((Button) findViewById(R.id.buttonToggleCaptureFirst)).setText("Stop");
                    ((TextView) findViewById(R.id.textViewStatus)).setText("Status: Capturing");
                } else {
                    ((Button) findViewById(R.id.buttonToggleCapture)).setText("Listen");
                    ((Button) findViewById(R.id.buttonToggleCaptureFirst)).setText("Listen");
                    ((TextView) findViewById(R.id.textViewStatus)).setText("Status: Idle");

                    // not nice, but works
                    if (checkStatus() == 2) findViewById(R.id.buttonDoneSetup).setEnabled(true);
                }
            }
        };

        ((Button) findViewById(R.id.buttonToggleCapture)).setOnClickListener(capture);
        ((Button) findViewById(R.id.buttonToggleCaptureFirst)).setOnClickListener(capture);

        ((TextView) findViewById(R.id.textViewStatus)).setText("Status: Idle");

        Button buttonTogglePlayback = (Button) findViewById(R.id.buttonTogglePlayback);
        buttonTogglePlayback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // ugly duplicated code
                if (mCapturingThread.capturing()){
                    mCapturingThread.stopCapturing();
                    ((Button) findViewById(R.id.buttonToggleCapture)).setText("Listen");
                    ((Button) findViewById(R.id.buttonToggleCaptureFirst)).setText("Listen");
                }

                if (mPlaybackThread == null || !mPlaybackThread.playing()) {

                    MultiAutoCompleteTextView messageEditText = (MultiAutoCompleteTextView) findViewById(R.id.multiAutoCompleteTextView);
                    int err = sendMessage(messageEditText.getText().toString(), CMD_MESSAGE);
                    if (err != 0) {
                        Toast.makeText(MainActivity.this, "Error! Could not send message. Err. code: "+String.valueOf(err),
                                Toast.LENGTH_LONG).show();
                        return;
                    }
                    messageEditText.setText("");
                    mPlaybackThread.startPlayback();
                } else {
                    mPlaybackThread.stopPlayback();
                }

                if (mPlaybackThread.playing()) {
                    ((Button) findViewById(R.id.buttonTogglePlayback)).setText("Stop");
                    ((TextView) findViewById(R.id.textViewStatus)).setText("Status: Playing audio");
                } else {
                    ((Button) findViewById(R.id.buttonTogglePlayback)).setText("Send");
                    ((TextView) findViewById(R.id.textViewStatus)).setText("Status: Idle");
                }
            }
        });

        Button buttonToggleKeys = (Button) findViewById(R.id.buttonToggleKeys);
        buttonToggleKeys.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // ugly duplicated code
                if (mCapturingThread.capturing()){
                    mCapturingThread.stopCapturing();
                    ((Button) findViewById(R.id.buttonToggleCapture)).setText("Listen");
                    ((Button) findViewById(R.id.buttonToggleCaptureFirst)).setText("Listen");
                }

                if (mPlaybackThread == null || !mPlaybackThread.playing()) {
                    if (sendMessage("",0) != 0 ) {
                        Toast.makeText(MainActivity.this, "Error! Could not send message.",
                                Toast.LENGTH_LONG).show();
                        return;
                    }
                    mPlaybackThread.startPlayback();
                } else {
                    mPlaybackThread.stopPlayback();
                }
            }
        });

        Button buttonToggleKeysFirst = (Button) findViewById(R.id.buttonToggleKeysFirst);
        buttonToggleKeysFirst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // ugly duplicated code
                if (mCapturingThread.capturing()){
                    mCapturingThread.stopCapturing();
                    ((Button) findViewById(R.id.buttonToggleCapture)).setText("Listen");
                    ((Button) findViewById(R.id.buttonToggleCaptureFirst)).setText("Listen");
                }

                if (mPlaybackThread == null || !mPlaybackThread.playing()) {
                    if (sendMessage("",0) != 0 ) {
                        Toast.makeText(MainActivity.this, "Error! Could not send message.",
                                Toast.LENGTH_LONG).show();
                        return;
                    }
                    mPlaybackThread.startPlayback();
                    buttonToggleKeysFirst.setEnabled(false);
                    if (checkStatus() == 2) findViewById(R.id.buttonDoneSetup).setEnabled(true);
                }
            }
        });
    }

    protected void onPause () {

        super.onPause();

        if (checkStatus() < 2) return;

        Log.v("ggwave", "Calling saveProfile");
        freeze();
        Toast.makeText(MainActivity.this, "Saving profile...",
                Toast.LENGTH_LONG).show();
        new Thread( new Runnable() { @Override public void run() {
            saveProfile(profileName, profilePass);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    unfreeze();
                }
            });
        } } ).start();
    }

    /*
    private String getRecordingFilePath()
    {
        Log.d("TAG", "Creating File for Audio");
        //I am using android 11

        ContextWrapper contextWrapper= new ContextWrapper(getApplicationContext());
        File musicDirectory = contextWrapper.getExternalFilesDir(Environment.DIRECTORY_MUSIC);
        File file = new File(musicDirectory,"testAudioFile.pcm");
        String path = file.getPath();
        Log.d("TAG", "File created at path:" + path);

        return path;
    }
    */
    private void startAudioCapturingSafe() {
        Log.i("ggwave", "startAudioCapturingSafe");

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED) {
            Log.i("ggwave", " - record permission granted");
            mCapturingThread.startCapturing();
        } else {
            Log.i("ggwave", " - record permission NOT granted");
            requestMicrophonePermission();
        }
    }

    private void requestMicrophonePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.RECORD_AUDIO)) {
            new AlertDialog.Builder(this)
                    .setTitle("Microphone Access Requested")
                    .setMessage("Microphone access is required in order to receive audio messages")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(MainActivity.this, new String[]{
                                    android.Manifest.permission.RECORD_AUDIO}, REQUEST_RECORD_AUDIO);
                        }
                    })
                    .setNegativeButton(android.R.string.no, null)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        } else {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{
                    android.Manifest.permission.RECORD_AUDIO}, REQUEST_RECORD_AUDIO);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_RECORD_AUDIO && grantResults.length > 0 &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            mCapturingThread.stopCapturing();
        }
    }
}

interface AudioDataReceivedListener {
    void onAudioDataReceived(short[] data);
}

class CapturingThread {
    private static final String LOG_TAG = CapturingThread.class.getSimpleName();
    private static final int SAMPLE_RATE = 48000;

    public CapturingThread(AudioDataReceivedListener listener) {
        mListener = listener;
    }

    private boolean mShouldContinue;
    private AudioDataReceivedListener mListener;
    private Thread mThread;

    public boolean capturing() {
        return mThread != null;
    }

    public void startCapturing() {
        if (mThread != null)
            return;

        mShouldContinue = true;
        mThread = new Thread(new Runnable() {
            @Override
            public void run() {
                capture();
            }
        });
        mThread.start();
    }

    public void stopCapturing() {
        if (mThread == null)
            return;

        mShouldContinue = false;
        mThread = null;
    }

    private void capture() {
        Log.v(LOG_TAG, "Start");
        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_AUDIO);

        // buffer size in bytes
        int bufferSize = AudioRecord.getMinBufferSize(SAMPLE_RATE,
                AudioFormat.CHANNEL_IN_MONO,
                AudioFormat.ENCODING_PCM_16BIT);
        Log.d("ggwave", "got buffer size " + bufferSize);
        if (bufferSize == AudioRecord.ERROR || bufferSize == AudioRecord.ERROR_BAD_VALUE) {
            bufferSize = SAMPLE_RATE * 2;
            Log.d("ggwave", "buffer size = " + bufferSize);
        } else {
            //Log.d("ggwave", "something is wrong");
            bufferSize = bufferSize * 2;
        }
        bufferSize = 4*1024;

        /*
        // Test recording
        int testBufferSize = Math.max(bufferSize/2, SAMPLE_RATE  * 10);
        Log.d("Fingerprinter", "bufferSize: " + testBufferSize);

        short[] audioData = new short[testBufferSize];

        // start recorder
        AudioRecord mRecordInstance = new AudioRecord(MediaRecorder.AudioSource.MIC,  SAMPLE_RATE, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT, bufferSize/2);

        mRecordInstance.startRecording();


        long time = System.currentTimeMillis();
        // fill audio buffer with mic data.
        int samplesIn = 0;
        do {
            samplesIn += mRecordInstance.read(audioData, samplesIn,
                    testBufferSize - samplesIn);

            if (mRecordInstance.getRecordingState() == AudioRecord.RECORDSTATE_STOPPED) {
                Log.d("Fingerprinter", "recording stopped");
                break;
            }
        } while (samplesIn < testBufferSize);
        Log.d("Fingerprinter",
                "Audio recorded: "
                        + (System.currentTimeMillis() - time)
                        + " millis");

        // see if the process was stopped.
        if (mRecordInstance.getRecordingState() == AudioRecord.RECORDSTATE_STOPPED) {
            // stica
        }
        // debugging: print audioData short[]
        Log.d("Fingerprinter", "Audio Data Content:");



        int sum = 0;

        for (int i : audioData)
            sum += i;
        Log.v(LOG_TAG, "fingerprint sum is " + sum);
        // String audioDataContent = "";
        Log.d("Fingerprinter", "samplesIn: " + samplesIn);




        mRecordInstance.stop();
        mRecordInstance.release();


        DataOutputStream dataOutputStream;
        try {
            dataOutputStream = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(kFilePath)));
            Log.d("TAG","recording to " + kFilePath);
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("TAG","Exception >>"+e.toString());
            return;
        }

//Saving file (Unable to Play it)
        try {
            for (int i = 0; i < audioData.length; i++ )
            {
                dataOutputStream.writeShort(audioData[i]);
            }

            Log.d("TAG","Writing file");

        } catch (Exception e) {
            Log.d("TAG","183 Eception>>"+e.toString());
            e.printStackTrace();
        }

        */



        // original implementqatio


        short[] audioBuffer = new short[bufferSize / 2];

        @SuppressLint("MissingPermission") AudioRecord record = new AudioRecord(MediaRecorder.AudioSource.MIC,
                SAMPLE_RATE,
                AudioFormat.CHANNEL_IN_MONO,
                AudioFormat.ENCODING_PCM_16BIT,
                bufferSize);

        Log.d("ggwave", "buffer size = " + bufferSize);
        Log.d("ggwave", "Sample rate = " + record.getSampleRate());

        if (record.getState() != AudioRecord.STATE_INITIALIZED) {
            Log.e(LOG_TAG, "Audio Record can't initialize!");
            return;
        }
        record.startRecording();

        Log.v(LOG_TAG, "Start capturing");

        long shortsRead = 0;
        while (mShouldContinue) {
            int numberOfShort = record.read(audioBuffer, 0, audioBuffer.length);
            //Log.v(LOG_TAG, "I read" + numberOfShort);

            //sum = 0;

            //for (int i : audioBuffer)
            //    sum += i;
            //Log.v(LOG_TAG, "sum is " + sum + " zero val is " + audioBuffer[0]);
            /*
            shortsRead += numberOfShort;
            for (int i = 0; i < numberOfShort; i++ ) {
                try {
                    dataOutputStream.writeShort(audioBuffer[i]);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            */

            mListener.onAudioDataReceived(audioBuffer);
        }

        record.stop();
        record.release();

        Log.v(LOG_TAG, String.format("Capturing stopped. Samples read: %d", shortsRead));
    }
}

interface PlaybackListener {
    void onProgress(int progress);
    void onCompletion();
}

class PlaybackThread {
    static final int SAMPLE_RATE = 48000;
    private static final String LOG_TAG = PlaybackThread.class.getSimpleName();

    public PlaybackThread(short[] samples, PlaybackListener listener) {
        mSamples = ShortBuffer.wrap(samples);
        mNumSamples = samples.length;
        mListener = listener;
    }

    private Thread mThread;
    private boolean mShouldContinue;
    private ShortBuffer mSamples;
    private int mNumSamples;
    private PlaybackListener mListener;

    public boolean playing() {
        return mThread != null;
    }

    public void startPlayback() {
        if (mThread != null)
            return;

        // Start streaming in a thread
        mShouldContinue = true;
        mThread = new Thread(new Runnable() {
            @Override
            public void run() {
                play();
            }
        });
        mThread.start();
    }

    public void stopPlayback() {
        if (mThread == null)
            return;

        mShouldContinue = false;
        mThread = null;
    }

    private void play() {
        int bufferSize = AudioTrack.getMinBufferSize(SAMPLE_RATE, AudioFormat.CHANNEL_OUT_MONO,
                AudioFormat.ENCODING_PCM_16BIT);
        if (bufferSize == AudioTrack.ERROR || bufferSize == AudioTrack.ERROR_BAD_VALUE) {
            bufferSize = SAMPLE_RATE * 2;
        }

        bufferSize = 16*1024;

        AudioTrack audioTrack = new AudioTrack(
                AudioManager.STREAM_MUSIC,
                SAMPLE_RATE,
                AudioFormat.CHANNEL_OUT_MONO,
                AudioFormat.ENCODING_PCM_16BIT,
                bufferSize,
                AudioTrack.MODE_STREAM);

        audioTrack.setPlaybackPositionUpdateListener(new AudioTrack.OnPlaybackPositionUpdateListener() {
            @Override
            public void onPeriodicNotification(AudioTrack track) {
                if (mListener != null && track.getPlayState() == AudioTrack.PLAYSTATE_PLAYING) {
                    mListener.onProgress((track.getPlaybackHeadPosition() * 1000) / SAMPLE_RATE);
                }
            }
            @Override
            public void onMarkerReached(AudioTrack track) {
                Log.v(LOG_TAG, "Audio file end reached");
                track.release();
                if (mListener != null) {
                    mListener.onCompletion();
                }
            }
        });
        audioTrack.setPositionNotificationPeriod(SAMPLE_RATE / 30); // 30 times per second
        audioTrack.setNotificationMarkerPosition(mNumSamples);

        audioTrack.play();

        Log.v(LOG_TAG, "Audio streaming started");

        short[] buffer = new short[bufferSize];
        mSamples.rewind();
        int limit = mNumSamples;
        int totalWritten = 0;
        while (mSamples.position() < limit && mShouldContinue) {
            int numSamplesLeft = limit - mSamples.position();
            int samplesToWrite;
            if (numSamplesLeft >= buffer.length) {
                mSamples.get(buffer);
                samplesToWrite = buffer.length;
            } else {
                for(int i = numSamplesLeft; i < buffer.length; i++) {
                    buffer[i] = 0;
                }
                mSamples.get(buffer, 0, numSamplesLeft);
                samplesToWrite = numSamplesLeft;
            }
            totalWritten += samplesToWrite;
            audioTrack.write(buffer, 0, samplesToWrite);
        }

        if (!mShouldContinue) {
            audioTrack.release();
        }

        Log.v(LOG_TAG, "Audio streaming finished. Samples written: " + totalWritten);
    }
}
